<?php

    interface LoggerInterface {
        public function logMessage($errorMassage);
        public function lastMessages($numberOfMessages);
    }

    interface EventListenerInterface {
        public function attachEvent($className, $callback);
        public function detouchEvent($className);
    }

    abstract class Storage implements LoggerInterface, EventListenerInterface {

        abstract public function create($object);
        abstract public function read($id);
        abstract public function update($id, $object);
        abstract public function delete($id);
        abstract public function list();

        public function logMessage($errorMassage)
        {
            // TODO: Implement logMessage() method.
        }
        public function lastMessages($numberOfMessages)
        {
            // TODO: Implement lastMessages() method.
        }
        public function attachEvent($className, $callback)
        {
            // TODO: Implement attachEvent() method.
        }
        public function detouchEvent($className)
        {
            // TODO: Implement detouchEvent() method.
        }
    }

    abstract class User implements EventListenerInterface {

        protected $id;
        protected $name;
        protected $role;

        abstract public function getTextsToEdit();

        public function attachEvent($className, $callback)
        {
            // TODO: Implement attachEvent() method.
        }
        public function detouchEvent($className)
        {
            // TODO: Implement detouchEvent() method.
        }
    }

class FileStorage extends Storage {
    public function create($object) {
        $slug = $object->slug;
        $date = date('Y-m-d');

        if (file_exists($slug . '_' . $date)) {
            $i = 1;
            while (file_exists($slug . '_' . $date . '_' . $i)) {
                $i++;
            }

            $slug = $slug . '_' . $date . '_' . $i;
            $object->slug = $slug;
        } else {
            $slug = $slug . '_' . $date;
            $object->slug = $slug;
        }

        file_put_contents($slug, serialize($object));

        return $slug;
    }

    public function read($id) {
        return unserialize(file_get_contents($id));
    }

    public function update($id, $object) {
        file_put_contents($id, serialize($object));

        return true;
    }

    public function delete($id) {
        unlink($id);

        return true;
    }

    public function list() {
        $files = scandir('.');

        $texts = [];

        foreach ($files as $file) {
            if (strpos($file, '.txt') !== false) {
                array_push($texts, unserialize(file_get_contents($file)));
            }
        }

        return $texts;
    }
}
